import Route from '@ioc:Adonis/Core/Route'

//Login para os tres tipos de usuários
Route.post('/login', 'AuthController.login');//PRESTE ATENÇÃO PARA NÃO INSERIR O NOME DO CONTROLLER ERRADO E NEM A FUNÇÃO
Route.post('/logout', 'AuthController.logout');//PRESTE ATENÇÃO PARA NÃO INSERIR O NOME DO CONTROLLER ERRADO E NEM A FUNÇÃO

//Cadastr de Clientes
Route.post("/cliente/cadastro", "ClientesController.store");//PRESTE ATENÇÃO PARA NÃO INSERIR O NOME DO CONTROLLER ERRADO E NEM A FUNÇÃO

//ROTAS PROTEGIDAS, SÓ PODE ACESSAR SE ESTIVER LOGADO
Route.group(() => {
  //ROTA MEUS DADOS
  Route.get('/auth/me', 'AuthController.me');
  //ROTA ATUALIZAR CLIENTE
  Route.put("/clientes", "ClientesController.update");//PRESTE ATENÇÃO PARA NÃO INSERIR O NOME DO CONTROLLER ERRADO E NEM A FUNÇÃO

}).middleware('auth');

Route.get('/', async () => {
  return { hortfruit: "Api" }
});

