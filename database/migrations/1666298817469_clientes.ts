import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'clientes'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table
      .integer("user_id")
      .unsigned()
      .notNullable()
      .references("id")
      .inTable("Users")
      .onDelete("CASCADE");
      table.string("nome", 255).notNullable();//nome
      table.string("telefone", 15).notNullable();//telefone
      table.timestamp("updated_at").nullable();
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
