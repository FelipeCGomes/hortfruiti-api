import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Estabelecimento from 'App/Models/Estabelecimento'
import User from 'App/Models/User'

export default class extends BaseSeeder {
  public async run() {
    // Write your database queries inside the run method
    const user = await User.create({
      email: 'estabelecimento@user.com',
      password: '123456',
      type: 'estabelecimentos',
    })

    await Estabelecimento.create({
      nome: 'Estabelecimento',
      logo: 'https://webevolui.com.br/principal/images/web-evolui-logo.png',
      online: true,
      bloqueado: false,
      userId: user.id,
    })
  }
}
