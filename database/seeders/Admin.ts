import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Admin from 'App/Models/Admin'
import User from 'App/Models/User'

export default class extends BaseSeeder {
  public async run() {
    // Esse método será mantido para futuras execuções

    const user = await User.create({
      email: 'admin@user.com',
      password: '123456',
      type: 'admins',
    })

    await Admin.create({
      nome: 'Admin',
      user_Id: user.id,
    })
  }
}
