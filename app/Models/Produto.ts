import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Produto extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public nome: string

  @column()
  public descricao: string //esse campo não pode ser nulo por enquanto

  @column()
  public posicao: number

  @column()
  public ativo: boolean

  @column()
  public imagem: string | null //esse campo pode ser nulo por enquanto

  @column()
  public preco: number

  @column()
  public unidade: string

  @column()
  public categoria_id: number


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public delectedAt: DateTime | null
}
