import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database';
import Cliente from 'App/Models/Cliente';
import User from 'App/Models/User';
import CreateClienteValidator from 'App/Validators/CreateClienteValidator';
import EditClienteValidator from 'App/Validators/EditClienteValidator';

export default class ClientesController {

    public async store({ request, response }: HttpContextContract) {
        //carregando a validação
        const payload = await request.validate(CreateClienteValidator);

        //utilizando a validação
        const user = await User.create({
            email: payload.email,
            password: payload.password,
            type: "clientes"

        });

        const cliente = await Cliente.create({
            nome: payload.nome,
            telefone: payload.telefone,
            user_Id: user.id,
        });

        return response.ok({
            id: cliente.id,
            nome: cliente.nome,
            email: user.email,
            telefone: cliente.telefone,
        });

    }

    public async update({ request, response, auth }: HttpContextContract) {
        const payload = await request.validate(EditClienteValidator);
        const userAuth = await auth.use("api").authenticate();

        //Criando uma transação para garantir que todas as alterações sejam feitas
        const trx = await Database.transaction();
        try {
            //buscando o usuário no db
            const user = await User.findByOrFail("id", userAuth.id);
            const cliente = await Cliente.findByOrFail("user_Id", userAuth.id);

            if (payload.password) {
                user.merge({
                    email: payload.email,
                    password: payload.password,
                });
            } else {
                user.merge({
                    email: payload.email,
                });
            }
            await user.save();

            cliente.merge({
                nome: payload.nome,
                telefone: payload.telefone,

            });
            await cliente.save();

            await trx.commit();
            return response.ok({
                id: cliente.id,
                nome: cliente.nome,
                email: user.email,
                telefone: cliente.telefone,
            });

        } catch (error) {
            await trx.rollback();
            return response.badRequest("Erro ao atualizar dados, tente novamente mais tarde");
        }






    }

}
