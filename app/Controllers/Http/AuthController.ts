import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Admin from 'App/Models/Admin';
import Cliente from 'App/Models/Cliente';
import Estabelecimento from 'App/Models/Estabelecimento';
import User from 'App/Models/User';

export default class AuthController {
    //Função Login
    public async login({ request, auth, response }: HttpContextContract) {
        const email = request.input('email');
        const password = request.input('password');

        try {
            const user = await User.findByOrFail('email', email);

            //token expira em...
            let expira;

            // type = tipo de usuário
            switch (user.type) {
                case "clientes":
                    expira = "30days";

                case "estabelecimentos":
                    expira = "7days";

                case "adminsistema":
                    expira = "1day";
                    break;
                default:
                    expira = "30days";
                    break;
            }
            const token = await auth.use("api").attempt(email, password, { expiresIn: expira, name: user.serialize().email, });

            response.ok(token);

        } catch {
            return response.badRequest('Invalid credentials');
        }
    }

    //Função Logout
    public async logout({ auth, response }: HttpContextContract) {
        try {
            await auth.use("api").revoke();
        } catch {
            return response.unauthorized("ERROR: Você não tem permissão para fazer isso!");
        }

        return response.ok(
            { revoked: true });
    }

    public async me({ auth, response }: HttpContextContract) {
        const userAuth = await auth.use("api").authenticate();

        let data;

        switch (userAuth.type) {
            case "clientes":
                const cliente = await Cliente.findByOrFail('user_id', userAuth.id);
                //criando um objeto e personalizando a resposta
                data = {
                    id_cliente: cliente.id,
                    nome: cliente.nome,
                    telefone: cliente.telefone,
                    email: userAuth.email,
                }
                break;
            case "estabelecimentos":
                const estabelecimento = await Estabelecimento.findByOrFail('user_id', userAuth.id);
                //criando um objeto e personalizando a resposta
                data = {
                    id_estabelecimento: estabelecimento.id,
                    nome: estabelecimento.nome,
                    logo: estabelecimento.logo,
                    online: estabelecimento.online,
                    bloqueado: estabelecimento.bloqueado,
                    email: userAuth.email,
                }
                break;
            case "admins":
                const admin = await Admin.findByOrFail('user_id', userAuth.id);
                //criando um objeto e personalizando a resposta
                data = {
                    id_admin: admin.id,
                    nome: admin.nome,
                    email: userAuth.email,
                }
                break;
            default:
                return response.unauthorized("ERROR: Você não tem permissão para fazer isso!(defaultCase)");

        }

        return response.ok(data);//a resposta
    }

}
